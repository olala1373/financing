package mrbapp.financing.interfaces

import mrbapp.financing.database.entities.Friends
import mrbapp.financing.database.entities.JoinFriendsFinanceEntity

interface IFriendListListener{
    fun onListItemClick(friend : JoinFriendsFinanceEntity)
    fun onCallClick(phone : String)
    fun onDetailsClick(friend: JoinFriendsFinanceEntity)
    fun onLongClickListItem(friend: JoinFriendsFinanceEntity)
}