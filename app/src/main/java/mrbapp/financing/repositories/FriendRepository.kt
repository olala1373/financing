package mrbapp.financing.repositories

import mrbapp.financing.database.AppDataBase
import mrbapp.financing.database.entities.Friends
import mrbapp.financing.models.Friend
import mrbapp.financing.ui.App

class FriendRepository {
    val friendDao = AppDataBase.getAppDataBase()?.friendDao()

    fun getAllFriends() = friendDao?.getFriends()

    suspend fun addFriend(friend : Friends) {
        friendDao?.insertFriend(friend = friend)
    }

    suspend fun deleteFriend(friend: Friends) {
        friendDao?.deleteFriend(friend)
    }

    suspend fun updateFriend(friend: Friends){
        friendDao?.updateFriend(friend)
    }

    fun getFriendFinanceList() = friendDao?.getFriendList()

    suspend fun getFriendById(id : Int) = friendDao?.getFriendById(id)
}