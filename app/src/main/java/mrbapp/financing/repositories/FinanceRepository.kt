package mrbapp.financing.repositories

import androidx.lifecycle.LiveData
import mrbapp.financing.database.AppDataBase
import mrbapp.financing.database.entities.Finance
import mrbapp.financing.ui.App

class FinanceRepository {
    val financeDao = AppDataBase.getAppDataBase(App.instance)?.financeDao()


    suspend fun getFinances() = financeDao?.getAllFinances()

    fun getFinancesLive() = financeDao?.getAllFinancesLive()

    fun getFinanceById(id : Int) = financeDao?.getFriendFinances(id)

    suspend fun insertFinance(finance : Finance) {
        financeDao?.insertFinance(finance)
    }
    fun getSumOfFinances(id : Int) = financeDao?.getTotalFinances(id)

    suspend fun updateFinance(finance: Finance) {
        financeDao?.updateFinance(finance)
    }

    suspend fun deleteFinance(finance: Finance){
        financeDao?.deleteFinance(finance)
    }
}