package mrbapp.financing.repositories

import android.util.Log
import mrbapp.financing.database.AppDataBase
import mrbapp.financing.database.entities.FriendAndFinance

class BackUpRepository {
    val friendDao = AppDataBase.getAppDataBase()?.friendDao()

    suspend fun backupAll() : List<FriendAndFinance>{
        val result = friendDao?.getFriendsAndFinances()
        Log.d("FUCK BACKUP" , "all : ${result?.first()?.finance}")
        return result ?: emptyList()
    }

    suspend fun backUpOne(id : Int) : FriendAndFinance?{
        val result = friendDao?.getFriendAndFinances(id)
        return result
    }
}

