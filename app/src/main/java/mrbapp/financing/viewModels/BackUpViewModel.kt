package mrbapp.financing.viewModels

import android.util.Log
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mrbapp.financing.database.entities.FriendAndFinance
import mrbapp.financing.database.entities.JoinFriendsFinanceEntity
import mrbapp.financing.repositories.BackUpRepository

class BackUpViewModel : ViewModel(){

    val repo = BackUpRepository()
    fun<T> convertGsonToJson(item : T) : String{
        val gson = Gson()
        val str = gson.toJson(item)
        Log.d("FUCK BACKUP" , "json : $str")
        return  str;
    }

    fun backUpAll(callback: (List<FriendAndFinance>) -> Unit) = CoroutineScope(Dispatchers.IO).launch {
        val result = repo.backupAll()
        callback.invoke(result)
    }

    fun backUpOne(id : Int , callback : (FriendAndFinance?) -> Unit) = CoroutineScope(Dispatchers.IO).launch {
        val result = repo.backUpOne(id)
        Log.d("FUCK BACKUP ONE" , result?.friend.toString())
        callback.invoke(result)
    }
}