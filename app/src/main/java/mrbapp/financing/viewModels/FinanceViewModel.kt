package mrbapp.financing.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mrbapp.financing.database.AppDataBase
import mrbapp.financing.database.dao.FinanceDao
import mrbapp.financing.database.entities.Finance
import mrbapp.financing.repositories.FinanceRepository
import mrbapp.financing.ui.App
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

open class FinanceViewModel : ViewModel(){
    var title = String()
    var amount : Int = 0
    var createdAt : Date = Calendar.getInstance().time
    var updatedAt : Date? = null

    val repository = FinanceRepository()


    fun getAllFinances() = CoroutineScope(Dispatchers.IO).launch {
        repository.getFinances()
    }


    fun getFinancesById(id : Int) : LiveData<List<Finance>>{
        return repository.getFinanceById(id)!!
    }

    fun addFinance(finance : Finance) = CoroutineScope(Dispatchers.IO).launch {
        repository.insertFinance(finance)
    }

    fun getSumOfFinances(id : Int) : LiveData<Int> = repository.getSumOfFinances(id)!!

    fun updateFinance(finance: Finance)= CoroutineScope(Dispatchers.IO).launch {
        repository.updateFinance(finance)
    }

    fun deleteFinance(finance : Finance) = CoroutineScope(Dispatchers.IO).launch {
        repository.deleteFinance(finance)
    }

}



