package mrbapp.financing.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mrbapp.financing.database.AppDataBase
import mrbapp.financing.database.dao.FriendsDao
import mrbapp.financing.database.entities.Friends
import mrbapp.financing.database.entities.JoinFriendsFinanceEntity
import mrbapp.financing.models.Friend
import mrbapp.financing.repositories.FriendRepository
import mrbapp.financing.ui.App
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

open class FriendViewModel : ViewModel(){
    var name : String = String()
    var phoneNumber : String = String()

    val repository = FriendRepository()

    fun getAllFriends() : LiveData<List<Friends>>{
        return repository.getAllFriends()!!
    }

    fun addFriend(friend: Friends) = CoroutineScope(Dispatchers.IO).launch {
        repository.addFriend(friend)
    }


    fun deleteFriend(friend: Friends) = CoroutineScope(Dispatchers.IO).launch{
        repository.deleteFriend(friend)
    }

    fun updateFriend(friend: Friends) = CoroutineScope(Dispatchers.IO).launch{
       repository.updateFriend(friend)
    }

    fun getFriendFinanceList() : LiveData<List<JoinFriendsFinanceEntity>>{
        return repository.getFriendFinanceList()!!
    }

    fun getFriendById(id : Int , callback : (Friends) -> Unit) = CoroutineScope(Dispatchers.IO).launch {
        callback(repository.getFriendById(id)!!)
    }


}