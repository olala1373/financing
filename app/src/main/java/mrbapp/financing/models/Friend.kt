package mrbapp.financing.models

data class Friend( var name : String , var phoneNumber : String , var totalDebts : Int)