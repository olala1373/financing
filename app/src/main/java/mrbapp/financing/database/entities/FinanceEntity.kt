package mrbapp.financing.database.entities

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import mrbapp.financing.database.DateTypeConverter
import java.util.*

@Entity(
    indices = [Index("amount")],
    foreignKeys = [ForeignKey(
        onDelete = CASCADE ,
        entity = Friends::class,
        parentColumns = ["id"],
        childColumns = ["friendId"]
    )]
)
data class Finance(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val friendId: Int,
    val amount: Int,
    val title: String? = null,
    val createdAt: Date,
    val updatedAt: Date? = null
)