package mrbapp.financing.database.entities

import androidx.room.Entity

@Entity
data class JoinFriendsFinanceEntity (
    var id : Int ,
    var name : String ,
    var phoneNumber : String? ,
    var debt : Int?)