package mrbapp.financing.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity
data class Friends(
    @PrimaryKey(autoGenerate = true)
    val id : Int? = null,
    @NotNull
    val name : String,

    val phoneNumber : String,
    val image : String
)