package mrbapp.financing.database.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation
import mrbapp.financing.models.Friend

class FriendAndFinance{
    @Embedded
    lateinit var friend : Friends
    @Relation(
        parentColumn = "id",
        entityColumn = "friendId"
    )
    var finance : List<Finance> = emptyList()
}

