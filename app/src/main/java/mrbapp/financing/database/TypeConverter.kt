package mrbapp.financing.database

import androidx.room.TypeConverter
import java.sql.Time
import java.util.*

class DateTypeConverter {

    @TypeConverter
    fun fromTimestamp(value :Long?) : Date?{
        return if (value === null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date : Date?) : Long?{
        return date?.time
    }
}