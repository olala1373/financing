package mrbapp.financing.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import mrbapp.financing.database.dao.FinanceDao
import mrbapp.financing.database.dao.FriendsDao
import mrbapp.financing.database.entities.Finance
import mrbapp.financing.database.entities.Friends
import mrbapp.financing.ui.App

@Database(entities = [Finance::class , Friends::class] , version = 4)
@TypeConverters(DateTypeConverter::class)
abstract class AppDataBase : RoomDatabase(){
    abstract fun financeDao() : FinanceDao
    abstract fun friendDao() : FriendsDao


    companion object {
        var INSTANCE : AppDataBase?= null

        private val MIGRATION_2_3 = object : Migration(2 , 3){
            override fun migrate(database: SupportSQLiteDatabase) {

            }

        }

        private val MIGRATION_3_4 = object : Migration(3 , 4){
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("Alter table Friends Add Column image TEXT not null default ''")
            }

        }

        fun getAppDataBase(context : Context = App.instance) : AppDataBase?{
            if (INSTANCE == null){
                synchronized(AppDataBase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext , AppDataBase::class.java , "FinancingDB").addMigrations(
                        MIGRATION_2_3 , MIGRATION_3_4)
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}