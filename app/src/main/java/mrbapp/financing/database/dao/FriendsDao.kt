package mrbapp.financing.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import mrbapp.financing.database.entities.FriendAndFinance
import mrbapp.financing.database.entities.Friends
import mrbapp.financing.database.entities.JoinFriendsFinanceEntity

@Dao
interface FriendsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFriend(friend : Friends)

    @Delete
    fun deleteFriend(friend: Friends)

    @Update
    fun updateFriend(friend: Friends)

    @Query("SELECT * FROM FRIENDS")
    fun getFriends() : LiveData<List<Friends>>

    @Query("SELECT * FROM FRIENDS WHERE name == :name")
    fun getFriendByName (name : String) : Friends

    @Query( "SELECT * FROM FRIENDS WHERE id == :id ")
    fun getFriendById(id : Int) : Friends

    @Query( "SELECT fr.id , fr.name , fr.phoneNumber , SUM(fi.amount) as 'debt' FROM Friends as fr LEFT JOIN Finance as fi on fr.id = fi.friendId group by fr.id ")
    fun getFriendList() : LiveData<List<JoinFriendsFinanceEntity>>

    @Transaction
    @Query("Select * From Friends")
    fun getFriendsAndFinances() : List<FriendAndFinance>

    @Transaction
    @Query("Select * From Friends where id =:id")
    fun getFriendAndFinances(id : Int) : FriendAndFinance

}