package mrbapp.financing.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import mrbapp.financing.database.entities.Finance

@Dao
interface FinanceDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFinance(finance: Finance)

    @Update
    fun updateFinance(finance: Finance)

    @Delete
    fun deleteFinance(finance: Finance)

    @Query("SELECT * FROM Finance")
    fun getAllFinances() : List<Finance>

    @Query("Select * From finance")
    fun getAllFinancesLive() : LiveData<List<Finance>>

    @Query ("SELECT * FROM Finance WHERE friendId == :friendId")
    fun getFriendFinances(friendId : Int) : LiveData<List<Finance>>

    @Query ("SELECT SUM(amount) from Finance WHERE friendId == :friendId")
    fun getTotalFinances(friendId: Int) : LiveData<Int>
}