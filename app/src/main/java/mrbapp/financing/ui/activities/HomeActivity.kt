package mrbapp.financing.ui.activities

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_home.*
import mrbapp.financing.R
import mrbapp.financing.adapters.FriendsAdapter
import mrbapp.financing.database.AppDataBase
import mrbapp.financing.database.entities.Friends
import mrbapp.financing.database.entities.JoinFriendsFinanceEntity
import mrbapp.financing.interfaces.IFriendListListener
import mrbapp.financing.ui.customViews.MyToolbar
import mrbapp.financing.ui.fragments.AddFriendDialogFragment
import mrbapp.financing.ui.fragments.FriendDetailDialogFragment
import mrbapp.financing.viewModels.BackUpViewModel
import mrbapp.financing.viewModels.FriendViewModel
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.io.File
import java.io.FileWriter
import java.io.OutputStreamWriter
import java.lang.NullPointerException
import kotlin.Exception

class HomeActivity : BaseActivity(), AddFriendDialogFragment.IAddDialogListener, IFriendListListener , FriendDetailDialogFragment.IOnDetailClickListener {


    private lateinit var model: FriendViewModel
    private lateinit var backUpModel : BackUpViewModel

    private val TAG: String = "FUCK HOME"
    private var db: AppDataBase? = null
    private var adapter: FriendsAdapter? = null
    private val addFriendDialogTag : String = "Add friend dialog"
    private val showDetailDialogTag : String = "show detail dialog"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        db = AppDataBase.getAppDataBase(context = this)

        model = ViewModelProviders.of(this).get(FriendViewModel::class.java)
        backUpModel = ViewModelProviders.of(this).get(BackUpViewModel::class.java)
        friends_list.layoutManager = LinearLayoutManager(this)

        friends_list.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if ( dy > 0 || dy < 0 && fab.isShown)
                    fab.hide()
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    fab.show()
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
        fetchDataToList()

        fab.onClick {
            showSimpleDialogFragment(AddFriendDialogFragment() , addFriendDialogTag)
        }

    }
    private fun fetchDataToList(){
        val data: List<JoinFriendsFinanceEntity> = emptyList()

        adapter = if (model.getAllFriends().value !== null)
            FriendsAdapter(model.getFriendFinanceList().value!! , this)
        else
            FriendsAdapter(data , this)

        friends_list.adapter = adapter

        model.getFriendFinanceList().observe(this, Observer<List<JoinFriendsFinanceEntity>> { friends ->
            adapter!!.setData(friends)
        })

        adapter?.let {
            it.onBackUpClick = {entity->
                backUpModel.backUpOne(entity.id) { res->
                    res?.let { item->
                        val strToBackUp = backUpModel.convertGsonToJson(item)
                        Log.d("FUCK BACKUP JSON" , strToBackUp)
                        backUpToFile("${entity.name}_backup" , strToBackUp)
                    }
                }
                backUpModel.convertGsonToJson(listOf(entity))
           }
        }

        toolbar.onLeftIconClick(object  : MyToolbar.OnLeftIconClickListener {
            override fun onClick() {
                backUpModel.backUpAll {
                    val str = backUpModel.convertGsonToJson(it)
                    backUpToFile("full_finance_backup" , str)
                }
            }
        })

    }

    private fun backUpToFile(name : String , data : String){
        try {
            val resolver = contentResolver
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME , name)
                put(MediaStore.MediaColumns.MIME_TYPE, "text/plain")
                put(MediaStore.MediaColumns.RELATIVE_PATH, "Documents")
            }
            val uri: Uri? = resolver.insert(MediaStore.Files.getContentUri("external"), contentValues)
            resolver.openOutputStream(uri!!).use {
                it?.write(data.toByteArray())
                it?.close()
            }

            runOnUiThread {
                Toast.makeText(this, "saved to : ${uri.toString()}", Toast.LENGTH_SHORT).show()
            }
        }catch ( e : Exception){
            e.printStackTrace()
        }
    }


    override fun onAddFriendClick() {
        val friends = Friends(name = model.name, phoneNumber = model.phoneNumber , image = "")
        model.addFriend(friends)
        dismissDialogFragment(addFriendDialogTag)
    }

    override fun onListItemClick(friend: JoinFriendsFinanceEntity) {
        Log.v(TAG , " ${friend.name}")
        val intent = Intent(this , FinanceActivity::class.java)
        intent.putExtra("ID" , friend.id)
        intent.putExtra("NAME" , friend.name)
        startActivity(intent)
    }

    override fun onCallClick(phone: String) {
        Log.v(TAG , " $phone")
    }

    override fun onDetailsClick(friend: JoinFriendsFinanceEntity) {
        showDetailDialog(friend)
    }

    override fun onLongClickListItem(friend: JoinFriendsFinanceEntity) {
        showDetailDialog(friend)
    }

    private fun showDetailDialog(friend: JoinFriendsFinanceEntity){
        val dialog = FriendDetailDialogFragment.newInstance(friend)
        dialog.show(supportFragmentManager , showDetailDialogTag)
    }

    override fun onDeleteClick(friendId: Int) {
        Log.v(TAG , "$friendId")
        model.getAllFriends().observe(this@HomeActivity , Observer<List<Friends>> {
            friends -> deleteFriend(friends , friendId)
        })
        try {
            dismissDialogFragment(showDetailDialogTag)
        }
        catch (e : Exception){
            e.printStackTrace()
        }
    }

    private fun deleteFriend(friends: List<Friends> , id : Int){
        val friend = friends.find { it.id == id }
        Log.v(TAG , "$friend")
        try {
            model.deleteFriend(friend!!)
        }
        catch (e : Exception){
            e.printStackTrace()
            //Toast.makeText(this , "در حال حاظر امکان حذف وجود ندارد", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onEditClick(friendId: Int , name : String , number: String) {
        val newFriend = Friends(friendId , name , number , "")
        try {
            model.updateFriend(newFriend)
            dismissDialogFragment(showDetailDialogTag)
        }
        catch (e : Exception){
            e.printStackTrace()
            //Toast.makeText(this , "در حال حاظر امکان ویرایش وجود ندارد", Toast.LENGTH_SHORT).show()
        }
    }


}
