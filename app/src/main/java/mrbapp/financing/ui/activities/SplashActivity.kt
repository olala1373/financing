package mrbapp.financing.ui.activities
import android.os.Bundle
import android.os.Handler
import mrbapp.financing.R

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            navigate(HomeActivity :: class.java , finish = true)
        }, 1000)

    }
}
