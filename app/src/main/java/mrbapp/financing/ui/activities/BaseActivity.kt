package mrbapp.financing.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.NavHostFragment
import mrbapp.financing.R
import java.lang.NullPointerException

abstract class BaseActivity : AppCompatActivity() {

    lateinit var hostNavigation : NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        hostNavigation = NavHostFragment.create(R.navigation.app_navigation)
    }

    fun <T> navigate(destination: Class<T>, finish: Boolean = false, id: Int? = null) {

        val intent = Intent(this, destination)
        if (id != null)
            intent.putExtra("ID", id)

        startActivity(intent)

        if (finish)
            finish()

    }

    @Throws(TypeCastException::class)
    fun dismissDialogFragment(tag : String){
        val dialog = supportFragmentManager.findFragmentByTag(tag) as DialogFragment
        dialog.dismiss()
    }

    fun showSimpleDialogFragment(dialogFragment : DialogFragment , tag : String){
        dialogFragment.show(supportFragmentManager , tag)
    }

}