package mrbapp.financing.ui.activities

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_finance.*
import kotlinx.android.synthetic.main.activity_finance.fab
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.my_toolbar_layout.*
import mrbapp.financing.R
import mrbapp.financing.adapters.FinanceAdapter
import mrbapp.financing.database.entities.Finance
import mrbapp.financing.ui.fragments.AddFinanceDialogFragment
import mrbapp.financing.viewModels.FinanceViewModel
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.sdk27.coroutines.onTouch
import java.lang.NullPointerException

class FinanceActivity : BaseActivity() , AddFinanceDialogFragment.IOnAddFinanceListener {
    var friendId : Int = 0
    lateinit var model : FinanceViewModel
    lateinit var adapter : FinanceAdapter
    lateinit var name : String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finance)
        friendId = intent.getIntExtra("ID" , 0)
        name = intent.getStringExtra("NAME")?:""
        toolbarTitle.text = name
        finance_list.layoutManager = LinearLayoutManager(this)
        model = ViewModelProviders.of(this).get(FinanceViewModel::class.java)

        fab.onClick {
            showDialog()
        }

        finance_list.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if ( dy > 0 || dy < 0 && fab.isShown)
                    fab.hide()
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    fab.show()
                super.onScrollStateChanged(recyclerView, newState)
            }

        })

        finance_list.onTouch { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN){
                fab.hide()
            }
            if (event.action == MotionEvent.ACTION_UP){
                fab.show()
            }

        }

        fetchDataToList()

    }

    private fun fetchDataToList(){
        val data : List<Finance> = emptyList()

        adapter = if (model.getFinancesById(friendId).value !== null)
            FinanceAdapter(this , model.getFinancesById(friendId).value!!)
        else
            FinanceAdapter(this , data)


        finance_list.adapter = adapter

        model.getFinancesById(friendId).observe(this , Observer<List<Finance>> {
            finance -> adapter.setData(finance)

            if (finance.isEmpty())
                empty_text.visibility = View.VISIBLE
            else
            empty_text.visibility = View.GONE
        })

        model.getSumOfFinances(friendId).observe(this , Observer<Int> {
            total -> debt_amount_txt.text = "$total"

            try {
                if (total >= 0){
                    overView_txt.text = getString(R.string.your_demand)
                }
                else{
                    overView_txt.text = getString(R.string.your_debt)
                }
            }
            catch (exception : NullPointerException){
                debt_amount_txt.text = "0"
                overView_txt.text = getString(R.string.your_demand)
            }


        })
    }

    private fun showDialog(){
        val dialog = AddFinanceDialogFragment()
        dialog.show(supportFragmentManager , "finance dialogFragment")
    }


    override fun onAddFinanceClick() {
        val dialog = supportFragmentManager.findFragmentByTag("finance dialogFragment") as DialogFragment
        dialog.dismiss()
        val finance = Finance(amount = model.amount , title = model.title , createdAt = model.createdAt , friendId = friendId)
        model.addFinance(finance)

    }
}
