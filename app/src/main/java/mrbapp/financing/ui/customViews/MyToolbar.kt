package mrbapp.financing.ui.customViews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.my_toolbar_layout.view.*
import mrbapp.financing.R
import org.jetbrains.anko.sdk27.coroutines.onClick

open class MyToolbar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr){

    lateinit var title : TextView
    lateinit var leftIcon : ImageView
    lateinit var rightIcon : ImageView
    lateinit var onRightIconClick : OnRightIconClickListener
    lateinit var onLeftIconClick : OnLeftIconClickListener


    init {

        attrs?.let {
            var typedArray = context.obtainStyledAttributes(it , R.styleable.MyToolbar)
            var titleText = typedArray.getString(R.styleable.MyToolbar_toolbar_title)
            var iconLeft = typedArray.getResourceId(R.styleable.MyToolbar_icon_left , 0)
            var iconRight = typedArray.getResourceId(R.styleable.MyToolbar_icon_right , 0)

            typedArray.recycle()

            var inflater : LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            inflater.inflate(R.layout.my_toolbar_layout , this)
            setTitle(titleText!!)
            setLeftIcon(iconLeft)
            setRightIcon(iconRight)


        }

    }


    fun setTitle( title : String){
        toolbarTitle.text = title
    }

    fun setRightIcon(icon : Int){
        right_icon.setImageResource(icon)
    }

    fun setLeftIcon(icon : Int){
        left_icon.setImageResource(icon)
    }

    fun onRightIconClick(listener: OnRightIconClickListener){
        onRightIconClick = listener
        right_icon.onClick {
            listener.onClick()
        }
    }

    fun onLeftIconClick(listener: OnLeftIconClickListener){
        onLeftIconClick = listener
        left_icon.onClick {
            listener.onClick()
        }
    }

    interface OnRightIconClickListener{
        fun onClick()
    }

    interface OnLeftIconClickListener{
        fun onClick()
    }
}