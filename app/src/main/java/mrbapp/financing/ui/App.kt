package mrbapp.financing.ui

import android.app.Application
import mrbapp.financing.extensions.DelegatesExt

class App : Application(){

    companion object {
        var instance : App by DelegatesExt.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}