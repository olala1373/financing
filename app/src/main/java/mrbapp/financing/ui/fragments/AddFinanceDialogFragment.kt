package mrbapp.financing.ui.fragments

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RadioButton
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.add_finance_fragment_layout.*
import mrbapp.financing.R
import mrbapp.financing.tools.ThousandsSeparator
import mrbapp.financing.viewModels.FinanceViewModel
import org.jetbrains.anko.db.INTEGER
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.toast
import java.lang.ClassCastException
import java.lang.Exception
import java.util.*

class AddFinanceDialogFragment : DialogFragment(){

    private lateinit var model: FinanceViewModel
    private lateinit var listener : IOnAddFinanceListener
    private val TAG : String = "FUCK ADD FINANCE DIALOG"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(FinanceViewModel::class.java)
        }?:throw Exception("Invalid activity")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_finance_fragment_layout , container , false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        finance_amount_edTxt.addTextChangedListener(ThousandsSeparator("," , "."))
    }

    override fun onResume() {
        super.onResume()
        add_finance_btn.onClick {
            if (checkValidation()){
                getValues()
                listener.onAddFinanceClick()
            }
        }
    }

    private fun checkValidation() : Boolean{
        return when {
            finance_amount_edTxt.text.isEmpty() ->{
                toast("لطفا مبلغ را وارد کنید")
                false
            }
            finance_title_edTxt.text.isEmpty()->{
                toast("لطفا عنوان را وارد کنید")
                false
            }
            else ->{
                true
            }
        }
    }

    private fun getValues(){
        var str : String = finance_amount_edTxt.text.toString()
        str = str.replace("," , "")
        with(model){
            amount = str.toInt()
            createdAt = Calendar.getInstance().time
            title = finance_title_edTxt.text.toString()
            var id = amount_status_radio.checkedRadioButtonId
            var radio : RadioButton = find(id)
            if (radio.text == getString(R.string.debt)){
                amount *= -1
            }
            Log.v(TAG , "amount = $amount - title = $title - created = $createdAt ")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var dialog =  super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return  dialog
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as IOnAddFinanceListener
        }
        catch (e : ClassCastException){
            throw ClassCastException((context.toString() + "problem with implementing the listener"))
        }
    }


    interface IOnAddFinanceListener{
        fun onAddFinanceClick()
    }
}