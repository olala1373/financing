package mrbapp.financing.ui.fragments

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.add_friend_fragment_layout.*
import mrbapp.financing.R
import mrbapp.financing.extensions.toast
import mrbapp.financing.viewModels.FriendViewModel
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.lang.ClassCastException

class AddFriendDialogFragment : DialogFragment() {

    private lateinit var model: FriendViewModel
    private lateinit var listener : IAddDialogListener
    private val TAG : String = "FUCK ADD FRIEND DIALOG"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(FriendViewModel::class.java)
        } ?: throw Exception("Invalid activity")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_friend_fragment_layout, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onResume() {
        super.onResume()
        add_friend_btn.onClick {
            model.name = friend_name_edTxt.text.toString()
            model.phoneNumber = friend_phone_edTxt.text.toString()
            Log.v(TAG , model.name)
            if (model.name.isEmpty())
                context?.toast("اسم باید وارد شود")
            else
            listener.onAddFriendClick()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as IAddDialogListener
        }
        catch (e : ClassCastException){
            throw ClassCastException((context.toString() + "problem with implementing the listener"))
        }
    }

    interface IAddDialogListener{
        fun onAddFriendClick()
    }


}