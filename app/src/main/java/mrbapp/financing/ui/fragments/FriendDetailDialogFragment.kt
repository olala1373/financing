package mrbapp.financing.ui.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.friend_detail_dialog_layout.*
import kotlinx.android.synthetic.main.friend_detail_dialog_layout.view.*
import mrbapp.financing.R
import mrbapp.financing.database.entities.JoinFriendsFinanceEntity
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.sdk27.coroutines.onTouch
import java.lang.Exception

class FriendDetailDialogFragment : DialogFragment() {

    private var friendId: Int = 0
    private lateinit var name: String
    private lateinit var mobile: String
    private var debt: Int = 0
    private lateinit var listener : IOnDetailClickListener

    companion object {
        private val EXTRA_FRIEND_ID = "extra_friend_id"
        private val EXTRA_FRIEND_NAME = "extra_friend_name"
        private val EXTRA_FRIEND_MOBILE = "extra_friend_mobile"
        private val EXTRA_FRIEND_DEBT = "extra_friend_debt"

        fun newInstance(friendFinance: JoinFriendsFinanceEntity): FriendDetailDialogFragment {
            val dialog = FriendDetailDialogFragment()

            val args = Bundle().apply {
                with(friendFinance) {
                    putInt(EXTRA_FRIEND_ID, id)
                    putString(EXTRA_FRIEND_NAME, name)
                    putString(EXTRA_FRIEND_MOBILE, phoneNumber)
                    if (debt == null)
                        putInt(EXTRA_FRIEND_DEBT, 0)
                    else
                        putInt(EXTRA_FRIEND_DEBT, debt!!)
                }
            }

            dialog.arguments = args

            return dialog
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments

        if (args != null) {
            with(args) {
                friendId = getInt(EXTRA_FRIEND_ID, 0)
                name = getString(EXTRA_FRIEND_NAME)!!
                mobile = getString(EXTRA_FRIEND_MOBILE)!!
                debt = getInt(EXTRA_FRIEND_DEBT , 0)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var root: View = inflater.inflate(R.layout.friend_detail_dialog_layout, container, false)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        root.friend_name_detail.setText(name)
        root.friend_number_detail.setText(mobile)
        if (mobile.isEmpty()){
            root.friend_number_detail.visibility = View.GONE
        }else {
            root.friend_number_detail.visibility = View.VISIBLE

        }
        if (debt < 0){
            debt = Math.abs(debt)
            root.debt_text_detail.text = "$debt تومان بدهکار "
            root.detail_dialog_wrapper.background = ContextCompat.getDrawable(activity!!.applicationContext , R.drawable.on_debt_round_corner)
        }
        else if (debt > 0){
            root.debt_text_detail.text = "$debt تومان طلبکار "
            root.setBackgroundResource(R.drawable.on_demand_round_corner)
        }
        else
            root.debt_text_detail.text = getString(R.string.no_debts_and_demands)

        root.delete_friend_btn.onClick {
            listener.onDeleteClick(friendId)
        }

        root.edit_friend_btn.onClick {
            root.friend_number_detail.isFocusable = true
            root.friend_number_detail.isClickable = true
            root.friend_number_detail.isFocusableInTouchMode = true
            root.friend_name_detail.isFocusable = true
            root.friend_name_detail.isClickable = true
            root.friend_name_detail.isFocusableInTouchMode = true
            root.friend_name_detail.requestFocus()

            root.friend_name_detail.background = ContextCompat.getDrawable(activity!!.applicationContext , R.drawable.edit_text_border)
            root.friend_number_detail.background = ContextCompat.getDrawable(activity!!.applicationContext , R.drawable.edit_text_border)

            Log.v("FUCK DETAIL" , "click on ${root.edit_friend_btn.text}")
            if (root.edit_friend_btn.text == getString(R.string.save)){
                listener.onEditClick(friendId , root.friend_name_detail.text.toString() , number = root.friend_number_detail.text.toString())
            }

            root.edit_friend_btn.text = getString(R.string.save)

        }

        return root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as IOnDetailClickListener
        }
        catch (e : Exception){
            e.printStackTrace()
        }
    }

    interface IOnDetailClickListener{
        fun onDeleteClick(friendId : Int)
        fun onEditClick(friendId: Int , name : String , number : String)
    }

}