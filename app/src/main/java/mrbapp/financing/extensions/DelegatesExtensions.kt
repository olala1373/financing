package mrbapp.financing.extensions

import android.app.Activity
import android.content.Context
import android.widget.Toast
import java.lang.IllegalStateException
import java.text.DecimalFormat
import kotlin.reflect.KProperty


object DelegatesExt {
    fun <T> notNullSingleValue() = NotNullSingleValueVar<T>()
}


class NotNullSingleValueVar<T>{
    private var value : T? = null

    operator fun getValue(thisRef : Any? , property : KProperty<*>): T = value ?:
            throw IllegalStateException("${property.name} not initialized")

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = if (this.value == null) value
        else throw IllegalStateException("${property.name} already initialized")
    }
}

fun Context.toast(message : String): Toast = Toast.makeText(this , message ,Toast.LENGTH_SHORT)

fun Double.formatPrice() : String{
    val df = DecimalFormat("#,###.##")
    return df.format(this)
}