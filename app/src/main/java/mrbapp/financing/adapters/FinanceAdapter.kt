package mrbapp.financing.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.finance_list_layout.*
import kotlinx.android.synthetic.main.finance_list_layout.view.*
import kotlinx.android.synthetic.main.friends_list_layout.*
import mrbapp.financing.R
import mrbapp.financing.database.entities.Finance
import mrbapp.financing.extensions.ctx
import mrbapp.financing.extensions.formatPrice
import mrbapp.financing.extensions.textColor
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.sdk27.coroutines.onClick
import saman.zamani.persiandate.PersianDate
import saman.zamani.persiandate.PersianDateFormat

class FinanceAdapter(val context: Context , private var financeList : List<Finance>) : RecyclerView.Adapter<FinanceAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.ctx).inflate(R.layout.finance_list_layout ,parent , false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int = financeList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindViews(financeList[position])

    }

    fun setData(newData : List<Finance>){
        this.financeList = newData
        notifyDataSetChanged()
    }


    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView) ,LayoutContainer {


        @SuppressLint("ResourceAsColor")
        fun bindViews(finance: Finance){
            with(finance){
                title_text.text = title
                val formatedPrice = amount.toDouble().formatPrice()
                amount_text.text = formatedPrice.toString()
                if (amount < 0){
                    amount_text.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context , R.color.debt_color))
                }
                else
                    amount_text.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context , R.color.demand_color))

                val pDate = PersianDate(createdAt)
                val formater = PersianDateFormat("j F y H:i")
                date_text.text = formater.format(pDate)

            }
        }
    }
}