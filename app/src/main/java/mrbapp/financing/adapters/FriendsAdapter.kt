package mrbapp.financing.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.friends_list_layout.*
import kotlinx.android.synthetic.main.friends_list_layout.debt_amount_txt
import kotlinx.android.synthetic.main.friends_list_layout.friend_name_txt
import kotlinx.android.synthetic.main.friends_list_layout.view.*
import kotlinx.android.synthetic.main.friends_list_layout2.*
import kotlinx.android.synthetic.main.friends_list_layout2.view.*
import mrbapp.financing.R
import mrbapp.financing.database.entities.Friends
import mrbapp.financing.database.entities.JoinFriendsFinanceEntity
import mrbapp.financing.extensions.ctx
import mrbapp.financing.extensions.formatPrice
import mrbapp.financing.interfaces.IFriendListListener
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.sdk27.coroutines.onLongClick
import java.lang.StringBuilder
import kotlin.math.abs

class FriendsAdapter(private var friendsList : List<JoinFriendsFinanceEntity> , private val context : Context) : RecyclerView.Adapter<FriendsAdapter.ViewHolder>(){

    private var listener : IFriendListListener = context as IFriendListListener
    var onDeleteClick : (JoinFriendsFinanceEntity) -> Unit = {}
    var onBackUpClick : (JoinFriendsFinanceEntity) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.ctx).inflate(R.layout.friends_list_layout2 , parent , false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int = friendsList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(friendsList[position])
        holder.itemView.onClick { listener.onListItemClick(friendsList[position]) }
       // holder.itemView.call_icon.onClick { listener.onCallClick(friendsList[position].phoneNumber!!) }
      //  holder.itemView.detail_icon.onClick { listener.onDetailsClick(friendsList[position]) }

        holder.itemView.setOnLongClickListener {
            listener.onLongClickListItem(friendsList[position])
            return@setOnLongClickListener true
        }
    }

    fun setData( newData : List<JoinFriendsFinanceEntity>){
        this.friendsList = newData
        notifyDataSetChanged()
    }


    inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView) , LayoutContainer {

        fun bindView(friend : JoinFriendsFinanceEntity){
            with(friend){
                friend_name_txt.text = name
                val firstLetter = name[0].toString()
                name_indicator.text = firstLetter
                if (debt == null || debt == 0)
                {
                    debt_amount_txt.text = context.getString(R.string.no_debt)
                    color_indicator.setImageResource(R.color.void_color)
                }
                else{
                    val str = StringBuilder("")
                    if (debt!! < 0){
                        str.append(context.getString(R.string.in_debt))
                        str.append(" ")
                        val formatedDebt = abs(debt!!).toDouble().formatPrice()
                        str.append(formatedDebt)
                        color_indicator.setImageResource(R.color.debt_color)
                    }
                    else{
                        str.append(context.getString(R.string.in_demand))
                        str.append(" ")
                        val formatedDebt = abs(debt!!).toDouble().formatPrice()
                        str.append(formatedDebt)
                        color_indicator.setImageResource(R.color.demand_color)
                    }
                    debt_amount_txt.text = str.toString()
                }
                backup_icon.onClick {
                    onBackUpClick.invoke(this@with)
                }
            }
        }
    }
}